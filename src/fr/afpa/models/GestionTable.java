package fr.afpa.models;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import fr.afpa.beans.Table;

public class GestionTable implements Flux {
	
	public void create(String nom, int nbColonne, String col) {
		Flux.create(new Table(nom, nbColonne, col));
	}
	
	public void insert(String tableName, String newData) {
		Table newTable = getTable(tableName);
		newTable.getData().add(newData);
		Flux.insert(newTable);

	}
	
	public String[] select(String tableName, String selection) {
		Table table = getTable(tableName);
		String dataToDisplay[] = new String [3];
		if ("*".equals(selection)) {
			dataToDisplay[0] = table.getCol();
			dataToDisplay[1] = "";
			for ( String str : table.getData()) {
				dataToDisplay[1] += str+"/";
			}
			dataToDisplay[2] = table.getNbColonne()+"";
			
		} else if (selection.indexOf(",")  != -1) {
			
			String [] selectColumn = selection.split(",");
			String [] column = table.getCol().split(";");
			String [] dataLine = new String[table.getNbColonne()];
			String [] goodDataLine = new String [table.getData().size()];
			String indexOfColumn = "";
			
			
			for ( int i = 0; i < goodDataLine.length; i++) {
				goodDataLine[i] ="";
			}
			for ( int i = 0; i < dataToDisplay.length; i++) {
				dataToDisplay[i] ="";
			}

			// On v�rifie quels indexs de colonne nous devons selectionner.
			for (int i = 0; i < selectColumn.length; i++) {
				for (int j = 0; j < column.length; j++ ){
					if (column[j].indexOf(selectColumn[i]) != -1) {
						indexOfColumn += j; 
						dataToDisplay[0] +=selectColumn[i]+";";
					}
				}
			}
			
			// On ins�re dans le tableau les indices de data correspondants aux colonnes choisies
			for (int i = 0; i < table.getData().size(); i++ ) {
				dataLine = table.getData().get(i).split(",");
				for(int j = 0 ; j < indexOfColumn.length(); j++) {
					goodDataLine[i] += dataLine[Integer.parseInt(indexOfColumn.charAt(j)+"")]+",";
				}
			}
			
			// On transforme le tableau goodDataligne en une seule ligne separ� par des "/" dans l'indice de dataToDisplay  
			for (int i = 0; i < goodDataLine.length; i++) {
				dataToDisplay[1] += goodDataLine[i]+"/"; 
			}
			dataToDisplay[2] = selectColumn.length+"";
			
		} else {
			
			dataToDisplay[0] = selection;
			String [] column = table.getCol().split(";");
			String indexOfColumn ="";
			String [] dataLine = new String[table.getNbColonne()];
			String [] goodDataLine = new String [table.getData().size()];
			
			dataToDisplay[2] = "1";
			
			 //initialisation pour eviter le null
			for ( int i = 0; i < goodDataLine.length; i++) {
				goodDataLine[i] ="";
			}
			
			for (int j = 0; j < column.length; j++ ){
				if (column[j].indexOf(dataToDisplay[0]) != -1) {
					indexOfColumn = j+""; 
				}
			}
			
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < table.getData().size(); i++ ) {
				dataLine = table.getData().get(i).split(",");
				goodDataLine[i] += dataLine[Integer.parseInt(indexOfColumn)]+",";	
			}
			for (int i = 0; i < goodDataLine.length; i++) {
				dataToDisplay[1] += goodDataLine[i]+"/"; 
			}
		}

		
		return dataToDisplay;
		
	}
	
	public String [] selectByOrder(String tableName, String selection, String orderByColumn, boolean asc) {
		Table table = getTable(tableName);
		String dataToDisplay[] = new String [3];
		int indexOrderByColumn = 0;
		String [] column = table.getCol().split(";");
		HashMap<String, String> dataBeforeTri = new HashMap<>();
		Map<String, String> dataTri;
		if (asc) {
			dataTri = new TreeMap<>();
		} else {
			dataTri = new TreeMap<>(Collections.reverseOrder());
		}

		
		if ("*".equals(selection)) {
			// R�cup�rer l'indice de la colonne a trier
			for ( int i = 0; i < column.length; i++) {
				if ( column[i].indexOf(orderByColumn) == 1){
					indexOrderByColumn = i;
					break;
				}
			}
			
			// r�cuperer dans un treemap la valeur de l'indice indexOrderByColumn de chaque string data en key et le string entier comme valeur
			for ( String str : table.getData()) {
				String [] dataLine = str.split(",");
				dataTri.put(dataLine[indexOrderByColumn], str);
			}
			
			//Mettre les valeur de datatri dans le tableau dataToDisplay[1] si croissant

			StringBuilder str = new StringBuilder();
			Set set = dataTri.entrySet();
			Iterator iterator = set.iterator();
			while(iterator.hasNext()) {
				Map.Entry<String, String> me = (Map.Entry)iterator.next();
				str.append(me.getValue()+"/");
			}

			// Affectation des valeurs souhait�es ( colonnes, data, nb colonnes;
			dataToDisplay[0] = table.getCol();
			dataToDisplay[1] = str.toString();
			dataToDisplay[2] = table.getNbColonne()+"";
		}
		
		return dataToDisplay;
			
	}
	
	/*
	 * BLOC DE METHODE : VERIFIE DANS LES DOSSIERS, FICHIERS OU COLONNES DONNES EN INSTRUCTION EXISTENT OU NON
	 */
	
	/**
	 * V�rifie dans le dossier BDD dont le chemin est indiqu� dans le fichier config si la table donn�e en instruction est d�j� cr��e
	 * @param table
	 * @return
	 */
	public static boolean tableIsExist(String table) {
		if (table.length() != 0) {
			// AFFICHAGE DU REPERTOIRE ENV ET LECTURE FICHIER
			System.out.println("la table"+table);
			File rep = new File(RB.getString("path.bdd"));
			if(rep.isDirectory()) {
				String [] t = rep.list();
				for (int i = 0; i < t.length; i++) {
					System.out.println(t[i]);
					if (t[i].equals(table)) {
						return true;
					}
				}
			}
		}
		System.out.println("false");
		return false;
	}
	
	/**
	 * R�cup�re les donn�es r�cupr�e dans Flux.getTable() et les transforme en instance de Table. Avec une list data remplie si la table contient des donn�es, ou null.
	 * @param table
	 * @return
	 */
	public Table getTable(String table) {
		String[] dataTable = Flux.getTable(table);
		Table newTable;
		ArrayList<String> data;
		
		if (dataTable[1].length() > 1) {
			String [] dataString = dataTable[1].split("/");
			data = new ArrayList(Arrays.asList(dataString));
			newTable = new Table(table, dataTable[0].split(";").length, dataTable[0], data);
		} else {
			newTable = new Table(table, dataTable[0].split(";").length, dataTable[0]);
		}
		return newTable;
	}
}
