package fr.afpa.models;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ResourceBundle;

import fr.afpa.beans.Table;

public interface Flux{

	static final ResourceBundle RB = ResourceBundle.getBundle("fr.afpa.props.config");
	static final String PATH = RB.getString("path.bdd");
	/**
	 * Permet de cr�er un fichier table avec le nom de l'objet Table comme nom de fichier
	 * @param table
	 */
	static void create(Table table) {
		String pathTable = RB.getString("path.bdd");
		BufferedWriter bw = null;
		File fichierTable = new File(pathTable+"//"+table.getNom());
		try {
			fichierTable.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			FileWriter fw = new FileWriter(fichierTable);
			bw = new BufferedWriter(fw);
			
			bw.write(table.getCol());
		}catch(Exception e) {
			
		} finally{
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Permet l'insertion des donn�es dans une table existante.
	 * @param table
	 */
	static void insert(Table table) {
		File rep = new File(RB.getString("path.bdd"));
		File fichierTable = new File(PATH+"//"+table.getNom());
		BufferedWriter bw = null;
		if(rep.isDirectory()) {
			String [] t = rep.list();
			for (int i = 0; i < t.length; i++) {
				if (t[i].equals(table.getNom())) {
					try {
						FileWriter fw = new FileWriter(fichierTable, true);
						bw = new BufferedWriter(fw);
						for (String str : table.getData() ) {
							bw.newLine();
							bw.write(str);
						}
					}catch(Exception e) {
						
					} finally{
						try {
							bw.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				break;
			}
		}
	}
	
	/**
	 * R�cup�re les donn�es �crites sur un fichier table en fonction de son nom donn� en parametre. 
	 * Renvoie un tableau String contenant le noms des colonnes et les data d�j� enregistr�es si elles existent.
	 * @param table
	 * @return
	 */
	static String[] getTable(String table) {
		FileReader fr = null;
		BufferedReader br = null;
		File fichierTable = new File(PATH+"//"+table);
		String column = "";
		String data = "";
		String[] dataTable = new String [2];

		try {
			fr = new FileReader(fichierTable);
			br = new BufferedReader(fr);
			column = br.readLine();
			while(br.ready()) {
				data += br.readLine()+"/";
			}
		}catch (IOException e) {
				e.printStackTrace();
			}finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		dataTable[0] = column;
		dataTable[1] = data;
		
		return dataTable;
	}

}
