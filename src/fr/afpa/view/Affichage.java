package fr.afpa.view;

import java.util.Scanner;

import fr.afpa.beans.Table;
import fr.afpa.controller.Controls;
import fr.afpa.models.GestionTable;

public class Affichage  {
	
	static Table table;
	static GestionTable gestionTable;
	static Controls controls;
	
	public Affichage() {
		controls = new Controls();
		gestionTable = new GestionTable();
		menu();
		
	}
	
	public void menu() {
		String instruction = displayBase();
		chooseErrorMessage(controls.control(instruction, gestionTable));
		menu();
	}
	
	
	public String displayBase() {
		System.out.println();
		System.out.println(">SQL");
		Scanner in = new Scanner(System.in);
		String instruction = in.nextLine();
		return instruction;
	}
	public void chooseErrorMessage(int retour) {
		if (retour == 0) {
			displaySyntaxError();
		} else if ( retour == 1) {
			displayTableExist();
		} else if (retour == 3) {
			displayErrorColumn();
		} else if (retour == 4) {
			displayTableDontExist();
		}else if (retour == 2) {
			displaySuccess();
		}
	}
	
	public void displayTableExist() {
		System.out.println("La table existe d�j�");
	}
	public void displayTableDontExist() {
		System.out.println("La table demand�e n'existe pas");
	}
	
	public void displayErrorColumn() {
		System.out.println("Le nombre de donn�es en entr�e ne correspond pas au nombre de colonne ");
	}
	
	public void displaySyntaxError() {
		System.out.println("Erreur de syntaxe");
	}
	
	public void displaySuccess() {
		System.out.println("Instruction bien r�alis�e");
	}
	
	public static void diplayTable(String [] dataToDisplay) {
		String [] column = dataToDisplay[0].split(";");
		String [] data = dataToDisplay[1].split("/");
		System.out.println("Data taille"+data.length);
		System.out.println("nb colonne "+Integer.parseInt(dataToDisplay[2]));
		int tailleCell = 0;
		int tailleCol = 0;
		// R�cuparation taille max des cellules
		for (int t = 0; t < data.length; t++) {
			if (data[t].length() > tailleCell) {
				tailleCell = data[t].length();
			}
		}
		for (int t = 0; t < column.length; t++) {
			if (column[t].length() > tailleCell) {
				tailleCol = column[t].length();
			}
		}
		if (tailleCell < tailleCol) {
			tailleCell = tailleCol;
		}

		
		displayContinuousLine(tailleCell, Integer.parseInt(dataToDisplay[2]));
		System.out.println();
		
		//Affichage ligne colonne
		for ( int i = 0; i < Integer.parseInt(dataToDisplay[2]); i++) {
			System.out.print("|"+column[i]);
			for (int j = 0; j < tailleCell-column[i].length(); j++ ) {
				System.out.print(" ");
			}
			
		}
		
		System.out.print("|");
		System.out.println();
		displayContinuousLine(tailleCell, Integer.parseInt(dataToDisplay[2]));
		
		System.out.println();
		// Affichage ligne(s) data
			
			for ( int i = 0; i < data.length; i++) {
				String [] dataLine = data[i].split(",");
				for ( int l = 0; l < dataLine.length; l++ ) {
					System.out.print("|"+dataLine[l]);
					for (int j = 0; j < tailleCell-dataLine[l].length(); j++ ) {
						System.out.print(" ");
					}
				}
				
				System.out.println("|");
				
			}

		
		displayContinuousLine(tailleCell, Integer.parseInt(dataToDisplay[2]));
	}
	
	
	
	/**
	 * Methode affichage des lignes continue
	 * @param args
	 */
	static void displayContinuousLine(int tailleCell, int nbColonne) {
		// 1ere ligne affichage
		for (int l = 0; l < nbColonne; l++ ) {
			System.out.print("+");
			for ( int l2 = 0; l2 < tailleCell; l2++) {
				System.out.print("-");
			}
		}
		System.out.print("+");
	}
	
	
	
	
	public static void main (String [] args) {
		Affichage affichage = new Affichage();
	}
	

}
