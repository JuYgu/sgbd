package fr.afpa.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Table implements Serializable{
	
	String nom;
	int nbColonne;
	String col;
	List<String> data;
	
	public Table(String nom, int nbColonne, String col) {
		this.nom = nom;
		this.nbColonne = nbColonne;
		this.col = col;
		data = new ArrayList<>() ;
	}
	
	public Table(String nom, int nbColonne, String col, ArrayList<String> data) {
		this.nom = nom;
		this.nbColonne = nbColonne;
		this.col = col;
		this.data = data;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbColonne() {
		return nbColonne;
	}

	public void setNbColonne(int nbColonne) {
		this.nbColonne = nbColonne;
	}

	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Table [nom=" + nom + ", nbColonne=" + nbColonne + ", col=" + col + ", data=" + data + "]";
	}
	
	
	
}
