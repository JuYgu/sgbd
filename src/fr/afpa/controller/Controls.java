package fr.afpa.controller;

import java.io.File;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import fr.afpa.beans.Table;
import fr.afpa.models.Flux;
import fr.afpa.models.GestionTable;
import fr.afpa.view.Affichage;

public class Controls implements Flux{

	static final ResourceBundle RB = ResourceBundle.getBundle("fr.afpa.props.config");
	
	public int control(String instruction, GestionTable gestionTable) {
		return analyseInstruction(instruction, gestionTable); 
	}
	
	public int analyseInstruction(String instruction, GestionTable gestionTable) {
		if ( analyseCreate(instruction)) {
			if (checkCreateInstruction(instruction, gestionTable)) {
				return 2;
			} else {
				return 1;
			}
		} 
		if (analyseInsert(instruction)) {
			return checkInsertInstruction(instruction, gestionTable);
		}
		if (analyseSelect(instruction)) {
			return checkSelectInstruction(instruction, gestionTable);
		}
		if (analyseUpdate(instruction)) {
			return checkUpdateInstruction(instruction, gestionTable);
		}
		return 0;
	}
	
	
	/*
	 * ------------------------------------------------BLOC DE METHODS CONCERNANT l'ANALYSE DE LA SYNTAXE--------------------------------------------------------
	 */
	
	/**
	 * Analyse la syntaxe et check si elle correspond a l'instruction CREATE
	 * @param instruction
	 * @return
	 */
	public boolean analyseCreate(String instruction) {
		try {
			Pattern p = Pattern.compile("CREATE TABLE [a-z]{1,25}\\(([a-z]{1,25}\\,?)+\\)\\;");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				return true;
			} 
		} catch(PatternSyntaxException pse) {}
		return false;
	}
	
	/**
	 * analyse la synstaxe et check si elle correspond a l'instruction INSERT
	 * @param instruction
	 * @return
	 */
	public boolean analyseInsert(String instruction) {
		try {
			Pattern p = Pattern.compile("INSERT INTO [a-z]{1,25} VALUES\\(('[a-z]{1,25}'\\,?)+\\)\\;");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				return true;
			} 
		} catch(PatternSyntaxException pse) {}
		return false;
	}
	
	/**
	 * Analyse la requete et check si sa syntaxe correspond bien a l'instruction SELECT
	 * @param instruction
	 * @return
	 */
	public boolean analyseSelect(String instruction) {
		try {
			Pattern p = Pattern.compile("SELECT (\\*|([a-z]{1,25},?)+) FROM [a-z]{1,25}(\\;| ORDER BY [a-z]{1,25} (ASC|DESC)\\;)");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				return true;
			} 
		} catch(PatternSyntaxException pse) {}
		return false;
	}
	
	
	public boolean analyseUpdate(String instruction) {
		try {
			Pattern p = Pattern.compile("UPDATE ([a-z]{1,25})+ SET [a-z]{1,25} = '[a-z]{1,25}'(\\;|(,[a-z]{1,25} = '[a-z]{1,25}',?)*\\;| WHERE [a-z]{1,25} = '[a-z]{1,25}'\\;)");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				return true;
			} 
		} catch(PatternSyntaxException pse) {}
		return false;
	}
	
	
	/*
	 * ---------------------BLOC DE METHODES CONCERNANT LA VERIFICATION DES INSTRUCTIONS ( SE DECLENCHENT APRES L'ANALYSE DE LA SYNTAXE )--------------------------
	 */
	
	/**
	 * v�rification de l'instruction CREATE,  v�rifie si la table donn�e en instruction existe ou non
	 * @param instruction
	 * @return
	 */
	public boolean checkCreateInstruction(String instruction, GestionTable gestionTable) {
		
		String table = getTableName(instruction, 1)+".cda";
		if (gestionTable.tableIsExist(table)) {
			return false;
		} else {
			String colonne = getColNames(instruction);
			gestionTable.create(table, colonne.split(";").length, colonne);
		}
		return true;
	}
	
	
	/**
	 * Verification de l'instruction INSERT - existence de la table demand�e, si les valeurs donn�es en ajout correspondent bien au nombre de colonne de la table
	 * @param instruction
	 * @param gestionTable
	 * @return
	 */
	public int checkInsertInstruction(String instruction, GestionTable gestionTable) {
		String table = getTableName(instruction, 2)+".cda";
		if(gestionTable.tableIsExist(table)) {
			String newData = getData(instruction);
			if (checkNbColumn(gestionTable.getTable(table), newData)) {
				gestionTable.insert(table, newData);
				return 2;
			} else {
				return 3;
			}	
		}
		return  4;
	}
	
	
	public int checkSelectInstruction(String instruction, GestionTable gestionTable) {
		String table = getTableName(instruction, 3)+".cda";
		if(gestionTable.tableIsExist(table)) {
			String selection = getSelection(instruction);
			if (instruction.indexOf("ORDER") == -1){
				String [] dataTable = gestionTable.select(table, selection);
				Affichage.diplayTable(dataTable);
				return 2;
			} else {
				String order = getOrderColumn(instruction);
				boolean asc = getOrder(instruction);
				String [] dataTable = gestionTable.selectByOrder(table, selection, order, asc);
				Affichage.diplayTable(dataTable);
				return 2;
			}
				
		}
		return 4;
	}
	
	public int checkUpdateInstruction(String instruction, GestionTable gestiontable) {
		String table = getTableName(instruction, 4)+".cda";
	}
	
	/*
	 * ----------------------------------BLOC DE METHODE : ISOLE LES INSTRUCTION ET RECUPERE LES STRING CORRESPONDANTS----------------------------------------
	 */

	/**
	 * isole la table donn�e en instruction
	 * @param instruction
	 * @return la chaine de caract�re repr�sentant la table a rechercher
	 */
	public String getTableName(String instruction, int type) {
		String table = "";
		String pattern = "";
		
		if (type == 1) {
			pattern = "[a-z]{1,25}\\(";
		} else if (type == 2) {
			pattern = "O [a-z]{1,25} V";
		} else if ( type == 3) {
			pattern = "[a-z]{1,25}(;| O)";
		} else if (type == 4) {
			pattern = "E [a-z]{1,25} S";
		}
		
		try { 
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				if (type == 1 || type == 3) {
					if (pattern.indexOf(";") != -1 || type == 1 ) {
						table = m.group().substring(0, m.group().length()-1);
						System.out.println("Dans le tri"+table+"b");
					} else {
						table = m.group().substring(0, m.group().length()-2);
						System.out.println(table);
					}

				} else if (type == 2 || type == 4) {
					table = m.group().substring(2, m.group().length()-2);
					System.out.println(table);
				}
			}
		} catch(PatternSyntaxException pse) {
			pse.printStackTrace();
		}
		return table;
	}
	
	/**
	 * isole et retourne les colonnes indiqu�es en paramettre
	 * @param instruction
	 * @return
	 */
	public String getColNames(String instruction) {
		String col = "";
		try {
			Pattern p = Pattern.compile("\\(([a-z]{1,25}\\,?)+\\)");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				col = m.group().substring(1, m.group().length()-1);
			} 
		} catch(PatternSyntaxException pse) {
			pse.printStackTrace();
		}
		
		if ( col.indexOf(',') != -1 ) {
			col = col.replaceAll("\\,", ";");
		}
		return col;
	}
	
	
	/**
	 * Retourne les donn�es a ins�rer dans la table
	 * @param instruction
	 * @return
	 */
	public String getData(String instruction) {
		String data = "";
		try {
			Pattern p = Pattern.compile("\\(('[a-z]{1,25}'\\,?)+\\)");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				data +=  m.group().substring(1, m.group().length()-1) + ",";
			}
		} catch(PatternSyntaxException pse) {
			pse.printStackTrace();
		}
		data = data.replaceAll("'", "");
		return data;
	}
	
	
	/**
	 * Renvoie pour la m�thode Select les champs selectionn�s par l'utilisateur pour l'affichage
	 * @param instruction
	 * @return
	 */
	public String getSelection(String instruction) {
		String selection = "";
		if (instruction.indexOf('*') != -1) {
			selection = "*";
		} else {
			try {
				Pattern p = Pattern.compile("T ([a-z]{1,25},?)+ F");
				Matcher m = p.matcher(instruction);
				if(m.find()) {
					selection +=  m.group().substring(2, m.group().length()-2);
				} 
			} catch(PatternSyntaxException pse) {
				pse.printStackTrace();
			}
		}
		return selection;
	}
	
	
	/**
	 * retourne la colonne avec laquelle l'int�gralit� des donn�es doivent �tre tri�es
	 * @param instruction
	 * @return
	 */
	public String getOrderColumn(String instruction) {
		String order = "";

			try {
				Pattern p = Pattern.compile("Y ([a-z]{1,25},?) (A|D)");
				Matcher m = p.matcher(instruction);
				if(m.find()) {
					order +=  m.group().substring(2, m.group().length()-2);
				} 
			} catch(PatternSyntaxException pse) {
				pse.printStackTrace();
			}
		return order;
	}
	
	public boolean getOrder(String instruction) {
		try {
			Pattern p = Pattern.compile("ASC");
			Matcher m = p.matcher(instruction);
			if(m.find()) {
				return true;
			} 
			
		} catch(PatternSyntaxException pse) {
			pse.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Permet de v�rifier si le nombre de donn�e a inserer dans la table correspond bien au nombre de colonne
	 * @param table
	 * @param data
	 * @return
	 */
	public boolean checkNbColumn(Table table, String data) {
		if (table.getNbColonne() == data.split(",").length) {
			return true;
		}else {
			return false;
		}
		
	}

}
